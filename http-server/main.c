//
//  main.c
//  http-server
//
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <dirent.h>
#include <libgen.h>
#include <assert.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <ctype.h>

#include "uthash.h"

#define BUFSIZE 4096

#define CACHE_MAX_RESPONSE_SIZE 4096

#define CACHE_ENABLED 1
/*
Max: getopt, server, directory management
Ilya: html, file find, autoindex, cgi
*/
struct cgiparam
{
    char *name;
    char *value;
};

struct filepath
{
    struct cgiparam *params;
    size_t paramsnum;
	char *workdir;   /*working directory*/
    char *file;      /*name of GET file*/
 	char *path;      /*relational path*/
	int isCGI;       /*0 or 1*/
};

struct cacheitem
{
    char *request;
    char response[CACHE_MAX_RESPONSE_SIZE];
    UT_hash_handle hh;
};


/* ========================================================= */
/* ================ CACHE TABLE METHODS ==================== */
/* ========================================================= */

int cache_add_request(char *request,char *response, struct cacheitem **caches)
{
    
    if (!CACHE_ENABLED) return 0;
    assert(strlen(response) < 4096);
    struct cacheitem *s = NULL;
    
    HASH_FIND_STR( *caches, request, s);
    if (s == NULL){
        
        s = malloc(sizeof(struct cacheitem));
        s->request = request;
        bzero(s->response, 4096);
        
        strcpy(s->response, response);
        HASH_ADD_KEYPTR( hh, *caches, s->request, strlen(s->request), s );
        
        free(response);
        
        return 0;
    }
    return -1;
}

void cache_get_request(char *request, struct cacheitem **caches,struct cacheitem **s)
{
    if (!CACHE_ENABLED) return;
    printf("\na\n");
    HASH_FIND_STR( *caches, request, *s);
}

void cache_clear(struct cacheitem **caches)
{
    if (!CACHE_ENABLED) return;
    struct cacheitem *s, *tmp;
    
    HASH_ITER(hh, *caches, s, tmp) {
        HASH_DEL(*caches, s);
        free(s->request);
        free(s);
    }
}

/* ========================================================= */
/* ================ CACHE TABLE METHODS END ================ */
/* ========================================================= */

int is_directory(const char *path)
{
    struct stat statbuf;
    if (lstat(path, &statbuf) != 0)
        return 0;
    return S_ISDIR(statbuf.st_mode);
}

char *generate_index(DIR *cur_dir,char *relative)
{
	struct dirent *read_dir = readdir(cur_dir);

	char *index = (char *)malloc(4096*sizeof(char));
	struct stat t;
	
	index = strcat(index, "<html><body>");
	while (read_dir != NULL)
	{
		lstat(read_dir->d_name, &t);
		
		index = strcat(index, "<a href=\"");
        index = strcat(index, relative);
		index = strcat(index, read_dir->d_name);
		index = strcat(index, "\">");
		index = strcat(index, read_dir->d_name);
		index = strcat(index, "</a><br>");
		index = strcat(index, "\n");

		read_dir = readdir(cur_dir);
	}
	index = strcat(index, "</body></html>");

	return index;
}

void generate_error(char **result, size_t *n, int errno)
{
    char *result_ = "<html>\n<body>\n<h1>\n\tError\n</h1>\n<p>\n\tHere is error info\n</p>\n<br>\n<button onclick=\"{alert(\"This is BUTTON!\");}\"></button>\n</body>\n</html>\n";
    *n = strlen(result_);
    
    *result = result_;
}

/* param1=1&param2=2&param3=3, for CGI */
void parse_params(char *str, struct cgiparam * result, size_t *nparams)
{
    char * chk = strtok(str,"&");
    
    //result = (struct cgiparam *) malloc(sizeof(struct cgiparam));
    *nparams = 0;
    while (chk != NULL) {
        char *name, *value;
        name = strtok(chk, "=");
        value = strtok(chk, NULL);
        struct cgiparam ts;
        ts.name = name;
        ts.value = value;
        struct cgiparam * tr = realloc(result, sizeof(struct cgiparam) * (++*nparams));
        result = tr;
        chk = strtok(str, NULL);
    }
};

struct filepath generate_filepath(const char *wd, const char * rpath){
    
    char *requestpath = malloc(strlen(rpath) + 1);
    strcpy(requestpath, rpath);
    
    for(int i = 0; requestpath[i]; i++){
        requestpath[i] = tolower(requestpath[i]);
    }
    
    struct filepath fileinfo;
    char *fullpath;
    
    fullpath = malloc(strlen(wd) + strlen(requestpath) + 1);
    bzero(fullpath, strlen(fullpath) + 1);
    strcat(fullpath, wd);
    strcat(fullpath, requestpath);
    
    /*if (access(fullpath, R_OK) == 0 && is_directory(fullpath) == 0){
        
        struct filepath ret = generate_filepath(wd,dirname(requestpath));
        free(fullpath);
        free(requestpath);
        return ret;
    }*/
    
    fileinfo.path = malloc(FILENAME_MAX);
    fileinfo.file = malloc(FILENAME_MAX);
    bzero(fileinfo.path, FILENAME_MAX);
    bzero(fileinfo.file, FILENAME_MAX);
      if (is_directory(fullpath)) {
            strcpy(fileinfo.path,requestpath);
        } else {
            strcpy(fileinfo.path, dirname(requestpath));
            strcpy(fileinfo.file,basename(requestpath));
        }
    if(fileinfo.path[strlen(fileinfo.path) - 1] != '/'){
        fileinfo.path[strlen(fileinfo.path)] = '/';
    }
    
    fileinfo.workdir = malloc(strlen(wd));
    strcpy(fileinfo.workdir,wd);
    
    fileinfo.isCGI = (strstr(requestpath, "/cgi-bin/") != NULL);
    fileinfo.paramsnum = 0;
    
    if (fileinfo.isCGI){
        char * victim = malloc(strlen(fileinfo.file));
        strcpy(victim, fileinfo.file);
        char * nfile = strtok(victim, "?");
        if (strcmp(nfile, fileinfo.file) != 0){
            char * toparse = strtok(victim, NULL);
            parse_params(toparse, fileinfo.params, &fileinfo.paramsnum);
        }
        
        free(victim);
    }
    
    free(fullpath);
    
    return fileinfo;
}

char *open_file(char *filename, size_t *n)
{
    FILE *f = fopen(filename, "rb");

    if (f == NULL){
        char *res;
        generate_error(&res, n, 404);
        return res;
    }
    
    fseek(f, 0, SEEK_END); /*to the end*/
    long fsize = ftell(f); /*count of bytes in file*/
    fseek(f, 0, SEEK_SET); /*to the beginning*/

    char *buf = malloc(fsize + 1);
    bzero(buf, fsize + 1);
    fread(buf, fsize, 1, f);
    fclose(f); 
    
	*n = strlen(buf);
    
    return buf;
}

int perform_request(char **result, size_t *n, struct filepath
                    request)
{
    char* fulldir = (char *)malloc((strlen(request.workdir)+strlen(request.path)+1));
	bzero(fulldir, strlen(request.workdir)+strlen(request.path)+1);
    strcat(fulldir, request.workdir);
    strcat(fulldir, request.path);
    
	DIR *dir1 = opendir(fulldir);
    DIR *dir2 = opendir(fulldir);
    
	struct dirent *read_dir = readdir(dir1);
	char found = 0, index_avail = 0; /*bool*/
	struct stat t;

	if (request.isCGI == 0) {
		while (read_dir != NULL)
		{
			if (lstat(read_dir->d_name, &t) == 0 || 1) {
                printf("%s===%s\n",read_dir->d_name,request.file);
				if (strcmp(read_dir->d_name, request.file) == 0) {
					found = 1;
                    char* fullfile = malloc(strlen(fulldir)+strlen(request.file)+1);
                    strcat(fullfile, fulldir);
                    strcat(fullfile, request.file);
        
					*result = open_file(fullfile, n);
                    
                    free(fullfile);
					break;
				}
				if (strcmp(read_dir->d_name, "index.html") == 0) 
					index_avail = 1;
				if (strcmp(read_dir->d_name, "index.htm") == 0)
					index_avail = 2;
			} else {
                printf("ERROR:%s===%s\n",read_dir->d_name,request.file);
				perror("perform_request(): lstat()");
			}
		
			read_dir = readdir(dir1);
		}
	} else {  /* if there is a cgi script */
		DIR *cgi_dir = opendir(fulldir);
		read_dir = readdir(cgi_dir);
		while (read_dir != NULL)
		{
				if (strcmp(read_dir->d_name, request.file) == 0) {
					found = 1;
					int cgi_pid = fork();
					int status;
                    char* fullfile = malloc(strlen(fulldir)+strlen(request.file)+1);
                    strcat(fullfile, fulldir);
                    strcat(fullfile, request.file);
                    if (cgi_pid == 0)
					{
						execl(fullfile,request.file, NULL);
					}
					wait(&status);
                    free(fullfile);
					break;
				}	
			
		
			read_dir = readdir(dir1);
		}
	}
	/* if no file with this name - returns index.html */
	if (found == 0) {
        if (index_avail == 1){
        char* fullfile = malloc(strlen(fulldir)+strlen("index.html")+1);
        strcat(fullfile, fulldir);
        strcat(fullfile, "index.html");
        
        *result = open_file(fullfile, n);
        
        free(fullfile);
        }else if (index_avail == 2){
            char* fullfile = malloc(strlen(fulldir)+strlen("index.htm")+1);
            strcat(fullfile, fulldir);
            strcat(fullfile, "index.htm");
            
            *result = open_file(fullfile, n);
            
            free(fullfile);
        }else {
			char *index = generate_index(dir2,request.path);
        	*result = index;
        	*n = strlen(index);
		}
	}
/*
	if (request.workdir != NULL)
		free(request.workdir);
	if (request.path != NULL)
		free(request.workdir);
	if (request.file != NULL)
		free(request.workdir);
*/
    closedir(dir1);
    closedir(dir2);
	return 200;
}


int server(unsigned short port, char* wd, struct cacheitem **cache)
{
    struct sockaddr_in serveraddr; /* server's addr */
    struct sockaddr_in clientaddr; /* client addr */
    struct hostent *hostp; /* client host info */
    char buf[BUFSIZE]; /* message buffer */
    char *hostaddrp; /* dotted decimal host addr string */
    unsigned int clientlen; /* byte size of client's address */
    int parentfd; /* parent socket */
    int childfd;  /* child socket */
    int optval;   /* flag value for setsockopt */
    ssize_t n;    /* message byte size */
    
    parentfd = socket(AF_INET, SOCK_STREAM, 0);
    
    if (parentfd < 0)
        return 2;

	optval = 1;
    setsockopt(parentfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval , sizeof(int));

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    /* this is an Internet address */
    serveraddr.sin_family = AF_INET;
    /* let the system figure out our IP address */
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    /* this is the port we will listen on */
    serveraddr.sin_port = htons(port);
    /* bind: associate the parent socket with a port */
    if (bind(parentfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0)
        return 3;
    /* listen: make this socket ready to accept connection requests */
    if (listen(parentfd, 5) < 0) /* allow 5 requests to queue up */
        return 4;
    /* main loop: wait for a connection request, echo input line, then close connection. */
    clientlen = sizeof(clientaddr);
    while (1) {
        /* accept: wait for a connection request */
        childfd = accept(parentfd, (struct sockaddr *)&clientaddr, &clientlen);
        if (childfd < 0)
            return 5;
        /* gethostbyaddr: determine who sent the message */
        //hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
        //                      sizeof(clientaddr.sin_addr.s_addr), AF_INET);
        //if (hostp == NULL)
        //    return 6; /*error("ERROR on gethostbyaddr");*/
        hostaddrp = inet_ntoa(clientaddr.sin_addr);/*???*/
        if (hostaddrp == NULL)
            return 7; /*error("ERROR on inet_ntoa");*/
        printf("server established connection with(%s)\n",/* hostp->h_name,*/ hostaddrp);
        /* read: read input string from the client */
        bzero(buf, BUFSIZE);
        n = read(childfd, buf, BUFSIZE);
        if (n < 0)
            return 8; /*error("ERROR reading from socket");*/
        printf("server received %zd bytes: %s", n, buf);
        
        char* requesttype;
        char* requestpath;
        char* result;
        size_t resultlen;
        
        requesttype = strtok(buf, " ");
        requestpath = strtok(NULL, " ");
        
        if (strcmp(requesttype, "GET") == 0) {
            struct cacheitem *citem = NULL;
            if (CACHE_ENABLED) {
                cache_get_request(requestpath, cache, &citem);
            }

            if (citem != NULL && CACHE_ENABLED) { // did find in cache
                resultlen = strlen(citem->response);
                result = malloc(resultlen);
                strcpy(result, citem->response);
                printf("===CACHE===\n%s\nLen:%ld\n", result, resultlen);
            } else { // did not find in cache
                struct filepath fileinfo = generate_filepath(wd, requestpath);
                printf("WD:%s\nPATH:%s\nFILE:%s\nisCGI:%d\n", fileinfo.workdir, fileinfo.path, fileinfo.file,fileinfo.isCGI);
                perform_request(&result, &resultlen, fileinfo);
                printf("===ANSWER===\n%s\nLen:%ld\n", result, resultlen);
            }
        } else {
            generate_error(&result, &resultlen, 503);
        }
        
        n = write(childfd, result, resultlen);
        
        if (resultlen < CACHE_MAX_RESPONSE_SIZE && CACHE_ENABLED) {
            cache_add_request(requestpath, result, cache);
        }
         
        if (n < 0){
            perror("Error writing to ");
            //return 9;
        }
        
        close(childfd);
    }
    
}

int main(int argc, char **argv)
{
    char* wd = NULL;
    int port = -1;
    int opt = -1;
    int serv;

    struct cacheitem * cache = NULL; /* Cache */
    
    while ((opt = getopt (argc, argv, "p:d:")) != -1) {
        switch (opt)
        {
            case 'd':
                wd = optarg;
                break;
            case 'p':
                port = strtol(optarg, NULL, 10);
                break;
        }
    }

    if ((is_directory(wd) && wd != NULL) == 0) {
        wd = getcwd(wd, PATH_MAX);
        printf("No good working directory given. Using default server path.");
    }
    printf("Working directory: %s\n", wd);
    if (access(wd, R_OK & W_OK) != 0) {
        perror("Failed to access path. Exiting...");
        return 403; /*Forbidden*/
    }

    if (port < 0)
        port = 8080;
    serv = server(port, wd, &cache);
    printf("Server returned %d", serv);
    
    
    cache_clear(&cache);
    free(wd);
end:
    return 0;
}
